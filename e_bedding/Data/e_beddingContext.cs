﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using e_bedding.Models;

namespace e_bedding.Models
{
    public class e_beddingContext : DbContext
    {
        public e_beddingContext (DbContextOptions<e_beddingContext> options)
            : base(options)
        {
        }

        public DbSet<e_bedding.Models.ItemsModel> Items { get; set; }

        public DbSet<e_bedding.Models.ChartModel> ChartModel { get; set; }

        public DbSet<e_bedding.Models.ReportModel> ReportModel { get; set; }
    }
}
