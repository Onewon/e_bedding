﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_bedding.Models
{
    public class ReportModel
    {
        [Display(Name = "Report ID")]
        public int ID { get; set; }
        [Display(Name = "Payment Method")]
        public string PaymentMethod { get; set; }
        [Display(Name = "Payment Price")]
        public decimal Price { get; set; }
        [Display(Name = "Released Time")]
        public string ReleasedTime { get; set; }
        [Display(Name = "User")]
        public string userEmail { get; set; }
    }
}