﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_bedding.Models
{
    public class ItemsModel
    {
        [Display(Name = "Item ID")]
        public int ID { get; set; }
        [Display(Name = "Item")]
        public string ItemName { get; set; }

        public string Type { get; set; }
        public string Color { get; set; }
        public string Band { get; set; }
        [Display(Name = "Price $")]
        public decimal Price { get; set; }
        public string Description { get; set; }

        [Display(Name = "Photo")]
        public string photo_url { get; set; }
    }
}