﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace e_bedding.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new e_beddingContext(

           serviceProvider.GetRequiredService<DbContextOptions<e_beddingContext>>()))
            {
                // Look for any movies.
                if (context.Items.Any())
                {
                    return; // DB has been seeded
                }
                context.Items.AddRange(
                new ItemsModel
                {
                    ItemName = "red Pillow",
                    Type = "Pillow",
                    Color = "red",
                    Band = "ABC",
                    Price = 12.50M,
                    Description = "a Pillow",
                    photo_url = "https://www.dormeo.co.uk/media/catalog/product/cache/ecd051e9670bd57df35c8f0b122d8aea/o/c/octasmart-pillow_1.jpg"
                },

                new ItemsModel
                {
                    ItemName = "white Duvet",
                    Type = "Duvet",
                    Color = "white",
                    Band = "EBD",
                    Price = 30.00M,
                    Description = "a Duvet",
                    photo_url = "https://cdn.shopify.com/s/files/1/1666/8247/products/Duvet-Aqua.jpg"
                }, 
                new ItemsModel
                {
                    ItemName = "Mattress Topper",
                    Type = "Topper",
                    Color = "white",
                    Band = "CBD",
                    Price =200M,
                    Description = "a Topper",
                    photo_url = "https://www.thefutonshop.com/media/catalog/product/cache/1/image/650x488/9df78eab33525d08d6e5fb8d27136e95/s/e/serenityplustopperchemicalfree_1.jpg"
                },
                new ItemsModel
                {
                    ItemName = "blue Beddings set",
                    Type = "Beddings set",
                    Color = "blue",
                    Band = "ABC",
                    Price = 5.99M,
                    Description = "a Beddings set",
                    photo_url = "https://easimmons.com/storage/2016/11/beddings-and-linen-6-1030x581.jpg"
                }
                 );
                context.SaveChanges();
            }
        }
    }
}
