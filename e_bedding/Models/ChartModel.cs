﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace e_bedding.Models
{
    public class ChartModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Chart ID")]
        public int ID { get; set; }
        [Display(Name = "ID")]
        public int ItemID { get; set; }
        [Display(Name = "Item Name")]
        public string ItemName { get; set; }
        [Display(Name = "Specification")]
        public string Specification { get; set; }
        public int Quantity { get; set; }
        [Display(Name = "Total Price")]
        public decimal Price { get; set; }
        [Display(Name = "User")]
        public string userEmail { get; set; }
    }
}