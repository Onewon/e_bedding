﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using e_bedding.Models;

namespace e_bedding.Controllers
{
    public class ChartController : Controller
    {
        private readonly e_beddingContext _context;

        public ChartController(e_beddingContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Manage()
        {
            return View(await _context.ChartModel.ToListAsync());
        }

        // GET: Chart
        public async Task<IActionResult> Index()
        {
            int var;
            var = int.Parse(DateTime.Now.ToString("yMddmmss"));
            var = var + 16666;
            ViewData["ReleasedID"] = var.ToString();
            String date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            ViewData["ReleasedTime"] = date;
            return View(await _context.ChartModel.ToListAsync());
        }

        // GET: Chart/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chartModel = await _context.ChartModel
                .FirstOrDefaultAsync(m => m.ID == id);
            if (chartModel == null)
            {
                return NotFound();
            }

            return View(chartModel);
        }

        // GET: Chart/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Chart/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,ItemName,Specification,Quantity,Price,userEmail")] ChartModel chartModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(chartModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(chartModel);
        }

        // GET: Chart/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chartModel = await _context.ChartModel.FindAsync(id);
            if (chartModel == null)
            {
                return NotFound();
            }
            return View(chartModel);
        }

        // POST: Chart/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,ItemName,Specification,Quantity,Price,userEmail")] ChartModel chartModel)
        {
            if (id != chartModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(chartModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChartModelExists(chartModel.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(chartModel);
        }

        // GET: Chart/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chartModel = await _context.ChartModel
                .FirstOrDefaultAsync(m => m.ID == id);
            if (chartModel == null)
            {
                return NotFound();
            }

            return View(chartModel);
        }

        // POST: Chart/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var chartModel = await _context.ChartModel.FindAsync(id);
            _context.ChartModel.Remove(chartModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ChartModelExists(int id)
        {
            return _context.ChartModel.Any(e => e.ID == id);
        }


        //      ID: index,
        //          ItemName: itemname,
        //        Specification: sepc,
        //        Quantity: qty,
        //        Price: (prc* qty).toFixed(2),
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(int ID,[Bind("ID,ItemID,ItemName,Specification,Quantity,Price,userEmail")] ChartModel chartModel)
        {
            //if (ID == chartModel.ID)
            //{
            //    if (ModelState.IsValid)
            //    {
            //        try
            //        {
            //            _context.Update(chartModel);
            //            await _context.SaveChangesAsync();
            //        }
            //        catch (DbUpdateConcurrencyException)
            //        {
            //            if (!ChartModelExists(chartModel.ID))
            //            {
            //                return NotFound();
            //            }
            //            else
            //            {
            //                throw;
            //            }
            //        }
            //        return RedirectToAction(nameof(Index));
            //    }
            //    return View(chartModel);
            //}
            if (ModelState.IsValid)
            {
                _context.Add(chartModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(chartModel);
        }

        //public IActionResult Get(int id, String ItemName, String Specification, int Quantity, float Price)
        //{
        //    //Flower = await _context.Flower.ToListAsync();
        //    //var _items = from i in _context.Items
        //    //             select i;
        //    //var _item = _items.Where(x => x.ID == id);
        //    //var items = await _item.ToListAsync();
        //    ViewData["msg"] = Quantity + " " + Specification + " " + ItemName + " are " + Price;
        //    return View();
        //}
    }
}
