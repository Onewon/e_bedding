﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Microsoft.AspNetCore.Hosting;

namespace e_bedding.Controllers
{
    public class UploadFilesController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public UploadFilesController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Upload()
        {
            return View();
        }
        public IActionResult UploadBlobPage()
        {
            return View();
        }

        [HttpPost("UploadFiles")]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            var filePath = Path.GetTempFileName();
            filePath = @"C:\Users\User\Desktop\blob_storage";
            CloudBlobContainer container = GetCloudBlobContainer();
            string webRootPath = _hostingEnvironment.WebRootPath;
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    var filename = formFile.FileName;
                    CloudBlockBlob blob = container.GetBlockBlobReference(filename);
                    //System.IO.File.OpenRead(@"<your desired source filepath>")
                    using (var fileStream = System.IO.File.OpenRead(filePath+ "\\"+filename))
                    {
                        blob.UploadFromStreamAsync(fileStream).Wait();
                    }
                }
            }
            //this appear to the same page
            //return "Success upload an item to the blob storage";
            return Ok("Success upload an item to the blob storage");
        }

        //create container page action
        public ActionResult CreateBlobContainer()
        {
            CloudBlobContainer container = GetCloudBlobContainer(); //to call the above function
            ViewBag.Success = container.CreateIfNotExistsAsync().Result;
            //create the container in storage
            ViewBag.BlobContainerName = container.Name;

            return View();
        }

        private CloudBlobContainer GetCloudBlobContainer()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            IConfigurationRoot Configuration = builder.Build();
            CloudStorageAccount storageAccount =CloudStorageAccount.Parse(Configuration["ConnectionStrings:AzureStorageConnectionString-1"]);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("test-blob-container");
            return container;
        }

        public ActionResult ListBlobs()
        {
            CloudBlobContainer container = GetCloudBlobContainer(); // get the container info
            List<string> blobs = new List<string>(); // create a list object to store the blob list
            BlobResultSegment resultSegment = container.ListBlobsSegmentedAsync(null).Result; // to get the list result
            foreach (IListBlobItem item in resultSegment.Results) // read the result
            {
                if (item.GetType() == typeof(CloudBlockBlob)) // this to check the type of blob
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    // get name and also url for photo in one line
                    blobs.Add(blob.Name + "#" + blob.Uri.ToString());
                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob blob = (CloudPageBlob)item;
                    blobs.Add(blob.Name + "#" + blob.Uri.ToString());
                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory dir = (CloudBlobDirectory)item;
                    blobs.Add(dir.Uri.ToString());
                }
            }
            return View(blobs);
        }

        public string UploadBlob()
        {
            CloudBlobContainer container = GetCloudBlobContainer();
            //choose the object from the computer and move to the blob storage
            /*container.GetBlockBlobReference("yellow_orchid.jpg") - 
            means when create item in blob storage, named it as yellow_orchid.jpg
            */
            CloudBlockBlob blob = container.GetBlockBlobReference("test.txt"); //targetName
            //System.IO.File.OpenRead(@"<your desired source filepath>")
            using (var fileStream = System.IO.File.OpenRead(@"C:\Users\User\Desktop\test.txt"))//path
            { //获得路径 连接 返回
                blob.UploadFromStreamAsync(fileStream).Wait();
            }

            //this appear to the same page
            return "Success upload an item to the blob storage";
        }

        public string DeleteBlob()
        {
            CloudBlobContainer container = GetCloudBlobContainer();
            CloudBlockBlob blob = container.GetBlockBlobReference("image2.jpg");
            var Success = blob.DeleteIfExistsAsync().Result;
            if (Success == false)
            {
                return "Unable to delete the object";
            }
            return "Success Delete The item from the BLob Storage!";
        }
    }
}