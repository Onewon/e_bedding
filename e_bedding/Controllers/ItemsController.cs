﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using e_bedding.Models;

namespace e_bedding.Controllers
{
    public class ItemsController : Controller
    {
        private readonly e_beddingContext _context;

        public ItemsController(e_beddingContext context)
        {
            _context = context;
        }
        public IList<ItemsModel> ITems { get; set; }
        public SelectList Types { get; set; }
        public string itemTypes { get; set; }
        public string SearchString { get; set; }

        public async Task<IActionResult> Manage()
        {
            return View(await _context.Items.ToListAsync());
        }

        // GET: Items/?category=id
        public async Task<IActionResult> Index(string category, string itemsType)
        { //string searchString, string itemType
          //
            if (String.IsNullOrEmpty(category))
            {
                ViewData["Message"] = "All eBedding Items";
            }
            else
            {
                ViewData["Message"] = "The " + category;
            }
            IQueryable<string> TypeQuery = from m in _context.Items
                                           orderby m.Band
                                           select m.Band;
            //Flower = await _context.Flower.ToListAsync();
            var ebditems = from m in _context.Items
                           select m;

            if (!String.IsNullOrEmpty(category))
            {
                ebditems = ebditems.Where(s => s.Type.Contains(category));
            }
            if (!String.IsNullOrEmpty(itemsType))
            {
                ebditems = ebditems.Where(x => x.Band == itemsType);
            }

            IEnumerable<SelectListItem> eitems = new SelectList(await TypeQuery.Distinct().ToListAsync());
            ViewBag.itemsType = eitems;
            ViewData["category"] = category;
            return View(await ebditems.ToListAsync());
        }



        // GET: Items/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemsModel = await _context.Items
                .FirstOrDefaultAsync(m => m.ID == id);
            if (itemsModel == null)
            {
                return NotFound();
            }

            return View(itemsModel);
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,ItemName,Type,Color,Band,Price,Description,photo_url")] ItemsModel itemsModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(itemsModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(itemsModel);
        }

        // GET: Items/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemsModel = await _context.Items.FindAsync(id);
            if (itemsModel == null)
            {
                return NotFound();
            }
            return View(itemsModel);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,ItemName,Type,Color,Band,Price,Description,photo_url")] ItemsModel itemsModel)
        {
            if (id != itemsModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(itemsModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemsModelExists(itemsModel.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(itemsModel);
        }

        // GET: Items/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemsModel = await _context.Items
                .FirstOrDefaultAsync(m => m.ID == id);
            if (itemsModel == null)
            {
                return NotFound();
            }

            return View(itemsModel);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var itemsModel = await _context.Items.FindAsync(id);
            _context.Items.Remove(itemsModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("ID,ItemName,Specification,Quantity,Price")] ChartModel chartModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(chartModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(chartModel);
        }

        private bool ItemsModelExists(int id)
        {
            return _context.Items.Any(e => e.ID == id);
        }

        private bool ChartModelExists(int id)
        {
            return _context.ChartModel.Any(e => e.ID == id);
        }

    }
}
