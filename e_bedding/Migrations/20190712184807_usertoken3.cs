﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace e_bedding.Migrations
{
    public partial class usertoken3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ItemID",
                table: "ChartModel",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemID",
                table: "ChartModel");
        }
    }
}
