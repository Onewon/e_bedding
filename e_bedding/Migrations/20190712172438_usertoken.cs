﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace e_bedding.Migrations
{
    public partial class usertoken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "userEmail",
                table: "ReportModel",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "userEmail",
                table: "ChartModel",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "userEmail",
                table: "ReportModel");

            migrationBuilder.DropColumn(
                name: "userEmail",
                table: "ChartModel");
        }
    }
}
